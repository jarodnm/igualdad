package navarro.jarod;

import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils;
import navarro.jarod.bl.Medico;
import navarro.jarod.bl.Paciente;
import navarro.jarod.bl.Persona;

import java.time.LocalDate;
import java.util.*;

import java.io.*;

public class UI {
    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    static PrintStream out = System.out;
    static ArrayList<Paciente> pacientes = new ArrayList<>();
    static ArrayList<Medico> medicos = new ArrayList<>();

    public static void main(String[] args) throws IOException {
        boolean noSalir = true;
        do {
            menu();
            out.println("Digite el la opción que guste");
            int opcion = Integer.parseInt(in.readLine());
            noSalir = ejecutarAccion(opcion);
        } while (noSalir);
    }

    public static void menu() throws IOException {
        out.println();
        out.println("1. Registrar Paciente");
        out.println("2. Registrar Médico");
        out.println("3. Lista de pacientes");
        out.println("4. Lista de medicos");
        out.println("5. Dar de alta");
        out.println("6. Salir");
    }

    public static boolean ejecutarAccion(int opcion) throws IOException {
        boolean noSalir = true;
        switch (opcion) {
            case 1:
                registrarPaciente();
                break;
            case 2:
                registrarMedico();
                break;
            case 3:
                listarPacientes();
                break;
            case 4:
                listarMedicos();
                break;
            case 5:
                darDeAlta();
                break;
            case 6:
                noSalir = false;
                out.println("Gracias por usar el programa");
                break;
            default:
                out.println("Digite una opción ");
                break;
        }
        return noSalir;
    }

    public static void registrarPaciente() throws IOException {
        if (medicos.isEmpty()) {
            out.println("No hay medicos registrados");
        } else {
            String nombre;
            String apellido;
            String cedula;
            LocalDate fechaIngreso;
            String doctor;
            out.println("Digite el nombre");
            nombre = in.readLine();
            out.println("Digite el apellido");
            apellido = in.readLine();
            out.println("Digite su cédula");
            cedula = in.readLine();
            fechaIngreso = LocalDate.now();
            doctor = asignarMedico();
            Paciente tmppaciente = new Paciente(nombre, apellido, cedula, fechaIngreso, doctor);
            pacientes.add(tmppaciente);
        }
    }

    public static void registrarMedico() throws IOException {
        String nombre;
        String apellido;
        String cedula;
        String numeroColegiado;
        String especialidad;
        out.println("Digite el nombre del médico");
        nombre = in.readLine();
        out.println("Digite los apellidos del médico");
        apellido = in.readLine();
        out.println("Digite la cédula del médico");
        cedula = in.readLine();
        out.println("Digite el numero de colegiado");
        numeroColegiado = in.readLine();
        out.println("Digite la especialidad");
        especialidad = in.readLine();
        Medico tmpmedico = new Medico(nombre, apellido, cedula, numeroColegiado, especialidad);
        medicos.add(tmpmedico);
    }

    public static void listarPacientes() throws IOException {
        for (Paciente dato : pacientes) {
            out.println(dato.toString());
        }
    }

    public static void listarMedicos() throws IOException {
        for (Medico dato : medicos) {
            out.println(dato.toString());
        }
    }

    public static String asignarMedico() throws IOException {
        Random r = new Random();
        String doctor;
        int aleatorio = r.nextInt(medicos.size());
        doctor = medicos.get(aleatorio).getNumeroColegiado();
        return doctor;
    }

    public static void darDeAlta()throws IOException{
        String numeroColegiado;
        String cedula;
        out.println("Digite su numero de colegiado");
        numeroColegiado = in.readLine();
        if(medicoExiste(numeroColegiado)){
            out.println("Digite la cédula del paciente");
            cedula = in.readLine();
            if(pacienteExiste(cedula)){
                if(pacienteAsignado(cedula,numeroColegiado)){
                    out.println("Felicidades su paciente ha sido dado de alta");
                }
            }
        }

    }

    public static boolean pacienteExiste (String cedula)throws IOException{
        boolean existe = false;
        for(Paciente dato : pacientes){
            if(dato.getCedula().equals(cedula)){
                existe = true;
            }
        }
        return existe;
    }
    public static boolean medicoExiste(String numeroColegiado)throws IOException{
        boolean existe = false;
        for(Medico dato : medicos){
            if(dato.getNumeroColegiado().equals(numeroColegiado)){
                existe = true;
            }
        }
        return existe;
    }

    public static boolean pacienteAsignado (String cedula , String numeroColegiado) throws IOException{
        boolean pacienteAsignado = false;
        int pos = 0;
        int posd = 0;
        for(Paciente datop : pacientes){
            if(datop.getCedula().equals(cedula)){
                if(datop.getDoctor().equals(numeroColegiado)){
                    pacienteAsignado = true;
                    pacientes.remove(pos);
                    break;
                }
            }
            pos++;
        }

        return pacienteAsignado;
    }


}


