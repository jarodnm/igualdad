package navarro.jarod.bl;

import java.time.LocalDate;
import java.util.Objects;

public class Paciente extends Persona {

    private LocalDate fechaIngreso;
    private String doctor;

    public Paciente() {
    super();
    }

    public Paciente(String nombre, String apellido, String cedula, LocalDate fechaIngreso, String doctor) {
        super(nombre, apellido, cedula);
        this.fechaIngreso = fechaIngreso;
        this.doctor = doctor;
    }

    public LocalDate getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(LocalDate fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public String getDoctor() {
        return doctor;
    }

    public void setDoctor(String doctor) {
        this.doctor = doctor;
    }

    @Override
    public String toString() {
        return
                super.toString()+", fechaIngreso: " + fechaIngreso +
                        ", doctor: " + doctor
                        ;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Paciente paciente = (Paciente) o;
        return Objects.equals(super.cedula, paciente.cedula);
    }


}
