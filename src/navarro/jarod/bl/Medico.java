package navarro.jarod.bl;

import java.util.Objects;

public class Medico extends Persona {
    private String numeroColegiado;
    private String especialidad;

    public Medico() {
      super();
    }

    public Medico(String nombre, String apellido, String cedula, String numeroColegiado, String especialidad) {
        super(nombre, apellido, cedula);
        this.numeroColegiado = numeroColegiado;
        this.especialidad = especialidad;
    }

    public String getNumeroColegiado() {
        return numeroColegiado;
    }

    public void setNumeroColegiado(String numeroColegiado) {
        this.numeroColegiado = numeroColegiado;
    }

    public String getEspecialidad() {
        return especialidad;
    }

    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }

    @Override
    public String toString() {
        return
                super.toString()+ ", numeroColegiado: " + numeroColegiado +
                        ", especialidad: " + especialidad
                      ;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Medico medico = (Medico) o;
        return Objects.equals(numeroColegiado, medico.numeroColegiado);
    }

}
